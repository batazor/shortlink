//go:generate wire
//+build wireinject
// The build tag makes sure the stub is not built in the final build.

/*
Billing Service DI-package
*/
package billing_di
