## NEXT-UI Application

This UI for shortlink application

#### Feature/Require

- NextJS (SSR/Static generate content)
- Auth (Kratos)
- Monitoring (Sentry)
- Pretty UI
  - tailwind CSS
  - Material-UI
- Pretty code base
  - Typescript
  - ESLint/Prettier

#### ENV

| Name            | Value   | Description |
| --------------- | ------- | ----------- |
| `SENTRY_ENABLE` | `false` | Init Sentry |
